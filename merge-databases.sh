#!/bin/bash

# backup complete
mysqldump -v --opt -h 138.197.15.186 --port=3311 --compress --events --routines --triggers --default-character-set=utf8 -u root --password=WbrBffEjcQ6d8yLhOrAwEVwq -B db_macsalud > ./backup_prod.sql

# backup routines and tables from dev
mysqldump -v --opt -h 192.168.71.200 -P 3311 --compress --events --tables --routines --triggers --no-data --skip-opt --default-character-set=utf8 -u root -pWbrBffEjcQ6d8yLhOrAwEVwq -B db_macsalud > ./backup_routines.sql

# drop database
mysql -h 127.0.0.1 -P 3306 --protocol=tcp -u root -padmin123 -e "drop database if not exists db_macsalud;"

# create database
mysql -h 127.0.0.1 -P 3306 --protocol=tcp -u root -padmin123 -e "create database if not exists db_macsalud;"

# restore routines and tables
mysql -h 127.0.0.1 -P 3306 -u root -padmin123 --database=db_macsalud < ./backup_routines.sql

# backup information from prod
mysqldump -v --opt -h 138.197.15.186 -P 3311 --default-character-set=utf8 -u root -pWbrBffEjcQ6d8yLhOrAwEVwq --skip-triggers --compact --no-create-info --complete-insert -B db_macsalud > ./backup_information.sql

# disable foreign keys, modified backup_infomation.sql
sed -i '1i SET FOREIGN_KEY_CHECKS=0;' ./backup_information.sql
sed -i '$a SET FOREIGN_KEY_CHECKS=1;' ./backup_information.sql

# restore information
mysql -h 127.0.0.1 -P 3306 -u root -padmin123 -B db_macsalud < ./backup_information.sql 
