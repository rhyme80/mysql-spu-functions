package main

import (
	"encoding/hex"
	"fmt"
	"golang.org/x/crypto/sha3"
)

func main() {
	h := sha3.NewLegacyKeccak256()
	h.Write([]byte("62409680"))
	p := hex.EncodeToString(h.Sum(nil))
	fmt.Println(p)
}
 
