# -----------------------------------------------------------------------------------

# AGREGAR----------------------------------------------------------------------------

create
    definer = root@`%` procedure sys_core_spu_denominacion_efectivo_guardar(IN tipo_moneda_ varchar(250),
                                                                            IN denominacion_ varchar(250),
                                                                            IN equivalencia_soles_ double,
                                                                            IN usuario_creacion_ int)
begin

    insert into core_denominacion_efectivo(id, tipo_moneda, denominacion, equivalencia_soles,
                                           usuario_creacion, fecha_creacion, eliminado)
    values (null,
            upper(tipo_moneda_),
            upper(denominacion_),
            equivalencia_soles_,
            usuario_creacion_,
            now(),
            false);

    select last_insert_id() as id from core_denominacion_efectivo limit 1;
end;

# ACTUALIZAR----------------------------------------------------------------------------

create
    definer = root@`%` procedure sys_core_spu_denominacion_efectivo_actualizar(IN id_ int,
                                                                               IN tipo_moneda_ varchar(250),
                                                                               IN denominacion_ varchar(250),
                                                                               IN equivalencia_soles_ double)
begin
    if exists(select 1 from core_denominacion_efectivo where id = id_ and eliminado = false)
    then
        update core_denominacion_efectivo
        set tipo_moneda=if(isnull(tipo_moneda_), tipo_moneda, upper(tipo_moneda_)),
            denominacion=if(isnull(denominacion_), denominacion, upper(denominacion_)),
            equivalencia_soles=if(isnull(equivalencia_soles_), equivalencia_soles, upper(equivalencia_soles_))
        where id = id_;
        select id_ as id;
    else
        select 0 as id_;
    end if;
end;

# ELIMINAR----------------------------------------------------------------------------

create
    definer = root@`%` procedure sys_core_spu_denominacion_efectivo_eliminar(IN id_ int)
begin
    if exists(select 1 from core_denominacion_efectivo where id = id_ and eliminado = false)
    then
        update core_denominacion_efectivo
        set eliminado = true
        where id = id_;

        select 1 as eliminado;
    else
        select 0 as eliminado;
    end if;
end;

# LISTAR----------------------------------------------------------------------------

create
    definer = root@`%` procedure sys_core_spu_denominacion_efectivo_listar()
begin
    select *
    from core_denominacion_efectivo
    where eliminado = false;
end;

# LISTAR GENERAL--------------------------------------------------------------------

create
    definer = root@`%` procedure sys_core_spu_denominacion_efectivo_listar_general()
begin
    select *
    from core_denominacion_efectivo;
end;

# RECUPERAR-------------------------------------------------------------------------

create
    definer = root@`%` procedure sys_core_spu_denominacion_efectivo_recuperar(IN id_ int)
begin
  select *
  from core_denominacion_efectivo
      where id=id_ and eliminado=false;
end;

